# Atlassian Client-side Extensions

A set of APIs that allow a plugin author to augment and extend an Atlassian Server product's runtime using client-side JavaScript code.

[TOC]

## Demo

This repository demonstrates how both product developers and plugin authors would make use of the APIs.

* `demos/demo-product` - Shows how a product developer could create a simple "app" with extension points for plugin developers to fill.
* `demos/demo-extensions-basic` - Shows how a plugin author would create extensions for the demo product.
* `demos/demo-extensions-complex` - Shows use of more advanced extension features, as well as illustrating some edge-cases.

Open the [Refapp Demo](./environments/refapp/) to see these various demos in action within an Atlassian product & plugin environment.

A storybook environment is "coming soon"™.

## Installation

If you want to provide Client-side Extensions capabilities in your product, please follow the [Installation Guide](INSTALLATION.md).

## Releases

For a full list of features released in each version, please read our [Change Log](CHANGELOG.md).

## Contributing

We encourage you to contribute to Client-side Extensions! Please check out the [Contributing](CONTRIBUTING.md) section to know how to get started.

# Creating an extension (Plugin authors)

Refer to the [documentation](https://developer.atlassian.com/server/framework/clientside-extensions/) to learn how to create extensions.

# Providing an extension point (Product developers)

First, make sure to install and provide Client-side Extensions in your Product as described in the [Installation Guide](INSTALLATION.md).

Next, add `@atlassian/clientside-extensions-components` to your project.
These are a set of React components and hooks that will help you use the APIs when your code is written in React.

### `useExtensions`

In order to provide a Location, import the `useExtensions` hook and use it as follow:

```jsx
import React from 'react';
import { useExtensions } from '@atlassian/clientside-extensions-components';

export default function LocationProvider() {
    // First item is an array of descriptors, second item is a boolean indicating if the registry has finished
    // loading the entry-points of the extensions for this extension-point.
    const extensions = useExtensions('product.location-name');

    return (
        <React.Fragment>
            {extensions.map(ext => (
                // ...render them as you need
            ))}
        </React.Fragment>
    )
}
```

What you receive is a list of descriptors that then you can render as you want. The shape of such descriptors is:

```ts
interface Descriptor {
    key: string;
    location: string;
    weight: number;
    params: {
        [key: string]: unknown;
    };
    attributes: {
        type: string;
        onAction?: () => void;
        [key: string]: unknown;
    };
}
```

You probably are most interested in the `attributes` property, which will be populated with the calculated attributes by
the Plugin System before returning them from the Hook.

Descriptors are delivered already sorted by `weight` (desc order), but you can always sort them differently before rendering.

### `useExtensionsLoadingState`

You can use this hook in order to indicate your users that extensions and their attributes are loading.

```jsx
import React from 'react';
import { useExtensions, useExtensionsLoadingState } from '@atlassian/clientside-extensions-components';

export default function LocationProvider() {
    const extensions = useExtensions('product.location-name');
    const loading = useExtensionsLoadingState('product.location-name');

    return (
        <React.Fragment>
            {loading ? 'loading...' : extensions.map(ext => (
                // ...render them as you need
            ))}
        </React.Fragment>
    )
}
```

### `useExtensionsUnsupported`

There might be cases where you want to support legacy WebItems which don't have an entry point with them.
You can make use of `useExtensionsUnsupported` hook to gather all those extensions, and then show them as you want.

```jsx
import React from 'react';
import { useExtensionsUnsupported } from '@atlassian/clientside-extensions-components';

export default function LegacyLocationProvider() {
    const extensions = useExtensionsUnsupported('product.legacy-location-name');

    return (
        <React.Fragment>
            {extensions.map(ext => (
                <a href="ext.url">{ext.label}</a>
            ))}
        </React.Fragment>
    );
}
```

### `useExtensionsAll`

You can use `useExtensionsAll` in case you need all the results from `useExtensions`, `useExtensionsLoadingState` and `useExtensionsLoadingState`
and prefer to have them in a single hook.

```jsx
import React from 'react';
import { useExtensionsAll } from '@atlassian/clientside-extensions-components';

export default function LocationProvider() {
    const [extensions, legacyPlugins, loading] = useExtensionsAll('product.location-name');

    const allPlugins - [...extensions, ...legacyPlugins].sort((a, b) => a.weight - b.weight);

    return (
        <React.Fragment>
            {loading ? 'loading...' : allPlugins.map(ext => (
                // ...render them as you need
            ))}
        </React.Fragment>
    );
}
```

## Context

You can share some context with the extensions rendered in a location too. This is specially useful when you want to have multiple
instances of a same location (e.g: a location inside the rows of a grid).

```jsx
import React from 'react';
import { useExtensions } from '@atlassian/clientside-extensions-components';

export default function LocationProvider({ context }) {
    // Pass your context as the second argument of the hook
    const [extensions, isLoading] = useExtensions('product.location-name', context);

    return (
        <React.Fragment>
            {extensions.map(ext => (
                // ...render them as you need
            ))}
        </React.Fragment>
    )
}
```

## Schemas and Plugin Location Info

In order to inform Plugin Devs which types of extensions and set of attributes you support in your locations, you need to define a Schema object.

A Schema is nothing more than a JSON Schema that looks as follow:

```json
// # schema.json
{
    "properties": {
        "type": {
            "type": "string",
            "description": "Plugin types supported",
            "enum": ["modal", "link", "button", "panel"]
        },
        "onAction": {
            "description": "Callback triggered on user interaction with the extension. Signature depends on extension type.",
            "type": "function"
        },
        "glyph": {
            "type": "string",
            "description": "Atlaskit Glyph name to render as an icon",
            "enum": ["cross", "check"]
        },
        "tooltip": {
            "type": "string",
            "description": "Tooltip content"
        },
        "hidden": {
            "type": "boolean",
            "description": "Hidden flag to hide the extension"
        },
        "disabled": {
            "type": "boolean",
            "description": "Disabled flag to disable the extension"
        },
        "loading": {
            "type": "boolean",
            "description": "Renders the extension in a loading state (if supported)"
        }
    },

    "required": ["type", "glyph", "tooltip"]
}
```

You can add or remove as many properties as you need, but don't forget to specify the types you support in your location
in order to avoid unhandled extensions.

Don't set `onAction` as required if you're accepting `Link` extensions, since they don't use the `onAction` API.

For now, the Plugin System will only validate that the `type` specified by a Plugin Dev is supported by your location.
In a future release, this Schema will also validate that the returned attributes are supported.

### `ExtensionPointInfo`

You can add this component to share with Plugin Devs the schema of your Locations, and also highlight the locations in
the current screen.

The information will only be available if the product has enabled their display in development mode.

```jsx
import React from 'react';
import { useExtensions, ExtensionPointInfo } from '@atlassian/clientside-extensions-components';

import schema from './schema.json';

export default function LocationProvider({ context }) {
    // Third param of `useExtensions` is a configuration object.
    // Add the schema as a property of it.
    const [extensions, isLoading] = useExtensions('product.location-name', context, { schema });

    return (
        <React.Fragment>
            <h4>
                Product Location
                <ExtensionPointInfo name="product.location-name" />
            </h4>
            {extensions.map(ext => (
                // ...render them as you need
            ))}
        </React.Fragment>
    )
}
```

Render the `ExtensionPointInfo` in a place that's visible even in edge cases, like when the location is inside a Dropdown
(place it in the dropdown trigger), or part of a Grid (place it in the header).

## Default extension types and handlers

### `Link`

Links are just a static set of attributes with at least a `url` and `label` property. You can then decide how to handle
the case of rendering links as you see fit.

```jsx
import React from 'react';
import { useExtensions, ExtensionPointInfo } from '@atlassian/clientside-extensions-components';
import { Button } from '@atlaskit/button';

import schema from './schema.json';

function LinkExtension({ ext }) {
    const { label, url, disabled } = ext.attributes;

    return (
        <Button href={url} isDisabled={disabled}>
            {label}
        </Button>
    );
}

function MyExtensionRenderer({ type, ext }) {
    switch (type) {
        case 'link':
            return <LinkExtension extension={ext} />;

        default:
            return null;
    }
}

export default function LocationProvider({ context }) {
    const [extensions, isLoading] = useExtensions('product.location-name', context, { schema });

    return (
        <React.Fragment>
            <h4>
                Product Location
                <ExtensionPointInfo name="product.location-name" />
            </h4>
            <ul>
                {extensions.map(ext => (
                    <li key={ext.key}>
                        <ExtensionRenderer extension={ext} />
                    </li>
                ))}
            </ul>
        </React.Fragment>
    );
}
```

### `Button`

Buttons provide a way for Plugin Devs to execute an action when their button is clicked.
They declare such action by adding an `onAction` method to their attributes, so you should bind that method to your
button click handler when rendering.

```jsx
import React from 'react';
import { useExtensions, ExtensionPointInfo } from '@atlassian/clientside-extensions-components';
import { Button } from '@atlaskit/button';

import schema from './schema.json';

function ButtonExtension({ ext }) {
    const { label, disabled, onAction } = ext.attributes;

    return (
        <Button isDisabled={disabled} onClick={onAction}>
            {label}
        </Button>
    );
}

function MyExtensionRenderer({ type, ext }) {
    switch (type) {
        case 'button':
            return <ButtonExtension extension={ext} />;

        default:
            return null;
    }
}

export default function LocationProvider({ context }) {
    const [extensions, isLoading] = useExtensions('product.location-name', context, { schema });

    return (
        <React.Fragment>
            <h4>
                Product Location
                <ExtensionPointInfo name="product.location-name" />
            </h4>
            <ul>
                {extensions.map(ext => (
                    <li key={ext.key}>
                        <MyExtensionRenderer extension={ext} />
                    </li>
                ))}
            </ul>
        </React.Fragment>
    );
}
```

### `Panel`

Panels provide a container for Plugin Devs to render custom HTML content (they are the equivalent of legacy web-panels).
In this case, the `onAction` method should receive an API with a `onMount` and `onUnmount` methods, and Products should handle
calling this methods when needed with the appropiate arguments.

Since this can be a bit difficult to mantain, we provide a default renderer that you can use.

```jsx
import React from 'react';
import { useExtensions, ExtensionPointInfo, PanelHandler } from '@atlassian/clientside-extensions-components';
import { Button } from '@atlaskit/button';

import schema from './schema.json';

const PanelExtension = ({ ext }) => {
    const { onAction } = ext.attributes;

    return <PanelHandler.PanelRenderer render={onAction} />;
};

function MyExtensionRenderer({ type, ext }) {
    switch (type) {
        case 'panel':
            return <PanelExtension extension={ext} />;

        default:
            return null;
    }
}

export default function LocationProvider({ context }) {
    const [extensions, isLoading] = useExtensions('product.location-name', context, { schema });

    return (
        <React.Fragment>
            <h4>
                Product Location
                <ExtensionPointInfo name="product.location-name" />
            </h4>
            <ul>
                {extensions.map(ext => (
                    <li key={ext.key}>
                        <MyExtensionRenderer extension={ext} />
                    </li>
                ))}
            </ul>
        </React.Fragment>
    );
}
```

### `Modal`

Modals provide a button that opens a modal, and then a container in the body of that modal to render custom content.
It also provides a set of APIs to interact with the modal.

Since this case is very complicated to handle, we recommend using the provided handler.

```jsx
import React from 'react';
import { useExtensions, ExtensionPointInfo, ModalHandler } from '@atlassian/clientside-extensions-components';
import { Button } from '@atlaskit/button';

import schema from './schema.json';

const ModalExtension = ({ ext }) => {
    const [isOpen, setIsOpen] = useState(false);

    const { onAction, ...attributes } = ext.attributes;
    return (
        <React.Fragment>
            <Button type="button" {...attributes} onClick={() => setIsOpen(true)}>
                {attributes.label}
            </Button>

            <ModalHandler.ModalRenderer isOpen={isOpen} onClose={() => setIsOpen(false)} render={onAction} />
        </React.Fragment>
    );
};

function MyExtensionRenderer({ type, ext }) {
    switch (type) {
        case 'modal':
            return <ModalExtension extension={ext} />;

        default:
            return null;
    }
}

export default function LocationProvider({ context }) {
    const [extensions, isLoading] = useExtensions('product.location-name', context, { schema });

    return (
        <React.Fragment>
            <h4>
                Product Location
                <ExtensionPointInfo name="product.location-name" />
            </h4>
            <ul>
                {extensions.map(ext => (
                    <li key={ext.key}>
                        <MyExtensionRenderer extension={ext} />
                    </li>
                ))}
            </ul>
        </React.Fragment>
    );
}
```

In case you want to provide your own modal, take a look at the default renderer for a guide on how to do so.

## Alternative to Hooks

A component to define a Location is also provided in case you prefere it over hooks:

```jsx
import React from 'react';
import { PluginLocation } from '@atlassian/clientside-extensions-components';

import schema from './schema.json';

const LinkExtension = ({ ext }) => {
    /*...*/
};

const ButtonExtension = ({ ext }) => {
    /*...*/
};

const PanelExtension = ({ ext }) => {
    /*...*/
};

const ModalExtension = ({ ext }) => {
    /*...*/
};

function MyExtensionRenderer({ type, ext }) {
    switch (type) {
        case 'link':
            return <LinkExtension extension={ext} />;

        case 'button':
            return <ButtonExtension extension={ext} />;

        case 'panel':
            return <PanelExtension extension={ext} />;

        case 'modal':
            return <ModalExtension extension={ext} />;

        default:
            return null;
    }
}

export default function LocationProvider({ context }) {
    return (
        <React.Fragment>
            <h4>
                Product Location
                <ExtensionPointInfo name="product.location-name" />
            </h4>
            <ul>
                <PluginLocation name="product.location-name" context={context} schema={schema}>
                    {extensions =>
                        extensions.map(ext => (
                            <li key={ext.key}>
                                <MyExtensionRenderer extension={ext} />
                            </li>
                        ))
                    }
                </PluginLocation>
            </ul>
        </React.Fragment>
    );
}
```

It uses hooks internally, so you will need to use a version of React which supports hooks.

