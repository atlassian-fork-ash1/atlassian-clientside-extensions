import { AsyncPanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point demo-product.extend-navigation
 */
export default AsyncPanelExtension.factory(() => {
    return {
        label: 'Tabs',
        section: 'basic.section',
        onAction() {
            return import(/* webpackChunkName: "demo-tabs" */ './05-tabs');
        },
    };
});
