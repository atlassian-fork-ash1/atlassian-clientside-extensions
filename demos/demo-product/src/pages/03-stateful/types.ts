export interface StatefulExtensionPointData {
    foo: string;
    randomNumber: number;
}
