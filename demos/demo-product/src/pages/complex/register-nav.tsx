import { AsyncPanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point demo-product.extend-navigation
 */
export default AsyncPanelExtension.factory(() => {
    return {
        label: 'Everything and the kitchen sink',
        section: 'complex.examples.demo',
        onAction() {
            return import(/* webpackChunkName: "demo-complex-page" */ './complex-page');
        },
    };
});
