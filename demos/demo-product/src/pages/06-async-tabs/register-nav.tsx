import { AsyncPanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point demo-product.extend-navigation
 */
export default AsyncPanelExtension.factory(() => {
    return {
        label: 'Async Tabs',
        section: 'basic.section',
        onAction() {
            return import(/* webpackChunkName: "demo-async-tabs" */ './06-async-tabs');
        },
    };
});
