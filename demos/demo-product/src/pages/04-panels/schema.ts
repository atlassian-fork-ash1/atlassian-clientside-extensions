export const schema = {
    type: 'object',
    properties: {
        type: {
            type: 'string',
            enum: ['panel'],
        },
        label: {
            type: 'string',
            description: 'A meaningful title for the rendered content',
        },
        onAction: {
            typeof: 'function',
            description: 'Callback will allow the plugin to define what happens when it is mounted and unmounted from the view',
        },
    },
};

export const contextSchema = {
    type: 'object',
    properties: {
        title: {
            type: 'string',
        },
    },
};
