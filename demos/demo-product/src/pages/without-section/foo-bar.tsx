import React, { FC } from 'react';
import { PanelExtension, renderElementAsReact } from '@atlassian/clientside-extensions';

const FooBar: FC = () => {
    return <section>Foooooooooo BAAAAAAAAR</section>;
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, FooBar);
};
