import { AsyncPanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point demo-product.extend-navigation
 */
export default AsyncPanelExtension.factory(() => {
    return {
        label: 'Foo Bar',
        onAction() {
            return import(/* webpackChunkName: "demo-async-tabs" */ './foo-bar');
        },
    };
});
