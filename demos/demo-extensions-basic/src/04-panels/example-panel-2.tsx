import React from 'react';
import { PanelExtension, renderElementAsReact } from '@atlassian/clientside-extensions';
import { ExtensionPointDataDemo04 } from '@atlassian/clientside-extensions-demo-product/src/pages/04-panels/types';

type MyComponentProps = PanelExtension.MountProps & {
    data: ExtensionPointDataDemo04;
};

let callCount = 0;

const MyCustomPanel = ({ data }: MyComponentProps) => {
    const { title } = data;
    callCount += 1;
    return (
        <p>
            The current call count is {callCount} at {title}
        </p>
    );
};

/**
 * @clientside-extension
 * @extension-point demo.04-panels
 */
export default PanelExtension.factory((api, data: ExtensionPointDataDemo04) => {
    return {
        label: 'Panel using React',
        onAction(panelApi: PanelExtension.Api) {
            renderElementAsReact(panelApi, MyCustomPanel, { data });
        },
    };
});
