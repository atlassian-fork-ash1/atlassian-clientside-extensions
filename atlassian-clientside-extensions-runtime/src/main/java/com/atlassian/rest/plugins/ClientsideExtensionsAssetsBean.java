package com.atlassian.rest.plugins;

import com.atlassian.plugin.web.api.WebItem;
import com.google.common.collect.Lists;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@XmlRootElement(name = "assets")
public class ClientsideExtensionsAssetsBean {
    @XmlElement
    private List<WebItemBean> webItems = Lists.newArrayList();

    public ClientsideExtensionsAssetsBean(Iterable<WebItem> items) {
        if (null != items) {
            for (WebItem item : items) {
                webItems.add(new WebItemBean(item));
            }
            webItems = sortWebItemsByWeight(webItems);
        }
    }

    private List<WebItemBean> sortWebItemsByWeight(List<WebItemBean> webItems) {
        return webItems.stream()
            .sorted(Comparator.comparing(WebItemBean::getWeight))
            .collect(Collectors.toList());
    }

    public List<WebItemBean> getWebItems() {
        return webItems;
    }

    public void setWebItems(List<WebItemBean> webItems) {
        this.webItems = webItems;
    }
}
