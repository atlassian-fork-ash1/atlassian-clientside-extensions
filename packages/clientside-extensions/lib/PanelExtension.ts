import { PanelHandler } from '@atlassian/clientside-extensions-components';
import { PluginAttributesProvider } from '@atlassian/clientside-extensions-registry';

export const factory = (provider: PluginAttributesProvider): PluginAttributesProvider => {
    return (...args) => {
        return {
            ...provider(...args),
            type: 'panel',
        };
    };
};

export type MountProps = PanelHandler.MountProps;
export class Api extends PanelHandler.Api {}
