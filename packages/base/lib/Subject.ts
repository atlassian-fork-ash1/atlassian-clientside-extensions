export type Observer<T> = (data: T) => void;
export interface Subscription {
    unsubscribe: () => void;
}

export interface Observable<T> {
    subscribe: (observer: Observer<T>) => Subscription;
}

/**
 * Subject for a single location and its observers
 *
 * Observers are callbacks provided by the different instances of a location that want to be notified
 * when a list of descriptors changed and is available to be rendered.
 */
export default class Subject<T> {
    protected observers: Observer<T>[] = [];

    /**
     * Notify all observers with a list of descriptors. Send the last value sent if no value is provided.
     */
    public notify(payload: T) {
        this.observers.forEach(observer => observer(payload));
    }

    /**
     * Add an observer to the location subject. The observer will be notified each time there's a new list of
     * descriptors ready to be renderer.
     */
    public subscribe(observer: Observer<T>): Subscription {
        this.observers.push(observer);

        return {
            unsubscribe: () => this.unsubscribe(observer),
        };
    }

    /**
     * Remove an observer from the location subject.
     */
    public unsubscribe(observer: Observer<T>) {
        const observerIndex = this.observers.indexOf(observer);

        this.observers.splice(observerIndex, 1);
    }

    /**
     * Converts a Subject into an Observable.
     * An observable is a subset of a subject, but it can only subscribe to values, not notify new value.
     */
    asObservable(): Observable<T> {
        return {
            subscribe: (observer: Observer<T>) => this.subscribe(observer),
        };
    }
}
