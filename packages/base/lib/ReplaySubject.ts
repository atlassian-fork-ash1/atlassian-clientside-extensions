import Subject, { Observer, Subscription } from './Subject';

/**
 * Subject for a single location and its observers
 *
 * Observers are callbacks provided by the different instances of a location that want to be notified
 * when a list of descriptors changed and is available to be rendered.
 */
export default class ReplaySubject<T> extends Subject<T> {
    private lastPayloads: T[] = [];

    private readonly replaySize: number;

    constructor(replaySize: number = 10) {
        super();
        this.replaySize = Math.abs(replaySize);
    }

    private addPayload(payload: T) {
        this.lastPayloads.push(payload);
        if (this.lastPayloads.length > this.replaySize) {
            this.lastPayloads = this.lastPayloads.slice(-this.replaySize);
        }
    }

    /**
     * @Override
     * Notify all observers with a list of descriptors. Send the last value sent if no value is provided.
     */
    public notify(payload: T) {
        this.addPayload(payload);
        super.notify(payload);
    }

    /**
     * @Override
     * Add an observer to the location subject. The observer will be notified each time there's a new list of
     * descriptors ready to be renderer.
     */
    subscribe(observer: Observer<T>): Subscription {
        this.lastPayloads.forEach(payload => {
            observer(payload);
        });

        return super.subscribe(observer);
    }
}
