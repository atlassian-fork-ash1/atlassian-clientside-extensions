import Subject from './Subject';

describe('Subject', () => {
    let subject: Subject<boolean>;

    beforeEach(() => {
        subject = new Subject();
    });

    it('should notify each observer when executed with the given payload', () => {
        // we expect 2 assertions to happen
        expect.assertions(2);

        const TEST_PAYLOAD = true;

        subject.subscribe(payload => {
            expect(payload).toBe(TEST_PAYLOAD);
        });
        subject.subscribe(payload => {
            expect(payload).toBe(TEST_PAYLOAD);
        });

        subject.notify(TEST_PAYLOAD);
    });

    it('should stop notifying unsubscribed observers', () => {
        expect.assertions(1);

        let count = 0;

        const subscription = subject.subscribe(() => {
            // this should not be called with the incremented count!
            if (count !== 0) {
                throw new Error('I was not unsubscribed!');
            }
            expect(count).toBe(0);
            subscription.unsubscribe();
        });

        subject.notify(true);
        count += 1;
        subject.notify(true);
    });

    it('should transform the subject into an observable', () => {
        expect.assertions(1);

        const TEST_PAYLOAD = false;

        subject.asObservable().subscribe(payload => {
            expect(payload).toBe(TEST_PAYLOAD);
        });

        subject.notify(TEST_PAYLOAD);
    });

    it('should start notifying descriptors once subscribed', () => {
        expect.assertions(1);
        const DO_NOT_EXPECT_THIS_PAYLOAD = false;
        const EXPECT_THIS_PAYLOAD = true;

        subject.notify(DO_NOT_EXPECT_THIS_PAYLOAD);
        subject.subscribe(payload => {
            expect(payload).toBe(EXPECT_THIS_PAYLOAD);
        });
        subject.notify(EXPECT_THIS_PAYLOAD);
    });
});
