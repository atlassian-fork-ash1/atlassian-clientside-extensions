import { onDebug } from '@atlassian/clientside-extensions-debug';
// eslint-disable-next-line import/no-extraneous-dependencies
import { PluginDescriptor, PluginAttributesProvider } from './types';

import LocationsSubject from './LocationSubject';
import { getLocationPlugins, getLocationResources } from './httpUtils';

/**
 * Registry of Client-side Extensions and Client Locations
 */
export default class ClientPluginsRegistry {
    /**
     * Map of locations and their subjects.
     */
    private locations: { [key: string]: LocationsSubject } = {};

    /**
     * List of plugins already fetched and ready to render.
     */
    private plugins: PluginDescriptor[] = [];

    /**
     * Internal cache to check which locations have received an initialization call
     */
    private initializedLocations: { [key: string]: boolean } = {};

    /**
     * Get a Location observable. The observer will be notified when new descriptors are available to render.
     */
    getLocation(locationName: string) {
        if (!locationName) {
            onDebug(({ warn }) => ({
                level: warn,
                message: 'No location name specified. Creating empty subject.',
                components: ['Registry'],
            }));
            const emptySubject = new LocationsSubject();
            // notify subject with an empty list and finish loading state
            emptySubject.notify({
                descriptors: [],
                loadingState: false,
            });
            return emptySubject.asObservable();
        }

        if (!this.locations[locationName]) {
            this.locations[locationName] = new LocationsSubject();

            // send a loading signal to the consumer to notify we're loading the descriptors and resources
            this.locations[locationName].notify({
                descriptors: [],
                loadingState: true,
            });

            getLocationPlugins(locationName)
                .then((pluginDescriptors: PluginDescriptor[]) => {
                    this.plugins.push(...pluginDescriptors);

                    // load resources for location and notify
                    getLocationResources(locationName).then(() => {
                        // In watch mode we can not assume we actually loaded the resources here,
                        // as the loaded resource merely loads the actual resources from the webpack-dev-server.
                        // This means a second call to the observers is likely to happen.
                        // However we an just artificially try to force it by deferring the call by some milliseconds
                        const deferTime = typeof module !== 'undefined' && ((module as unknown) as { hot: boolean }).hot ? 1500 : 0;
                        setTimeout(() => {
                            // only initialize if not yet initialized.
                            if (!this.initializedLocations[locationName]) {
                                this.locations[locationName].notify({
                                    descriptors: pluginDescriptors,
                                    loadingState: false,
                                });
                            }
                        }, deferTime);
                    });
                })
                .catch(e => {
                    onDebug(({ error }) => ({
                        level: error,
                        message: `Failed to load plugins for location ${locationName}`,
                        components: ['Registry'],
                        meta: {
                            location: locationName,
                            error: e,
                        },
                    }));
                    this.locations[locationName].notify({
                        descriptors: [],
                        loadingState: false,
                    });
                });
        }

        // Return the Observable right away and let the Fetch notify the LocationSubject when the descriptors are fetched.
        return this.locations[locationName].asObservable();
    }

    /**
     * Extension entry-points use this API to register their attribute providers.
     */
    registerExtension(key: string, attributesProvider: PluginAttributesProvider) {
        const pluginDescriptor = ClientPluginsRegistry.getPluginsByKey(key, this.plugins);

        if (!pluginDescriptor) {
            onDebug(({ error }) => ({
                level: error,
                message: `Received register call for unregistered plugin with key: "${key}".
No such plugin was previously registered.
Please make sure to correctly specify your plugin in the atlassian-plugin.xml`,
                components: ['Registry'],
                meta: {
                    key,
                },
            }));
            return;
        }

        pluginDescriptor.attributesProvider = attributesProvider;

        // due to the fact that we will load all resources at once
        // we can assume that all registerExtension calls for a location come at the same time
        // this - combined with the 'deferred' calling nature of the `notify` method
        // means we do not have to wait for the last registerExtension call
        const loadingState = false;

        // set location as initialized.
        this.initializedLocations[pluginDescriptor.location] = true;

        this.locations[pluginDescriptor.location].notify({
            descriptors: this.getPluginsByLocation(pluginDescriptor.location),
            loadingState,
        });
    }

    private getPluginsByLocation(locationName: string): PluginDescriptor[] {
        return this.plugins.filter(descriptor => descriptor.location === locationName);
    }

    private static getPluginsByKey(key: string, pluginDescriptors: PluginDescriptor[]): PluginDescriptor {
        return pluginDescriptors.find(descriptor => descriptor.key === key);
    }
}
