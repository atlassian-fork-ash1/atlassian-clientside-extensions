import { renderElement } from 'atlassian-webresource-webpack-plugin/src/helpers/xml';
import renderCondition from 'atlassian-webresource-webpack-plugin/src/renderCondition';
import { ClientsideExtensionsOptions } from '../types';

export default (options: ClientsideExtensionsOptions): string => {
    return renderElement(
        'web-item',
        {
            key: options.key,
            section: options.section,
            weight: options.weight,
        },
        [renderElement('label', { key: options.label }), renderCondition(options.condition as {})],
    );
};
