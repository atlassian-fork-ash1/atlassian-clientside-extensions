// eslint-disable-next-line import/no-extraneous-dependencies
import { JSONSchema7 } from 'json-schema';
import Ajv from 'ajv';
import ajvKeywords from 'ajv-keywords';

// eslint-disable-next-line import/prefer-default-export
export const validateSchema = (schema: JSONSchema7, objectUnderTest: object) => {
    const ajv = new Ajv({ allErrors: true, $data: true });
    ajvKeywords(ajv, 'typeof');
    const validate = ajv.compile(schema as BufferSource);

    const valid = validate(objectUnderTest);
    if (!valid) {
        throw new Error(ajv.errorsText(validate.errors, { separator: '\n\t\t' }));
    }
};
