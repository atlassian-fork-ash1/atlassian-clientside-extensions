import { Context, PluginDescriptor } from '@atlassian/clientside-extensions-registry';
import { onDebug } from '@atlassian/clientside-extensions-debug';
import React from 'react';
import { Options, useExtensionsAll } from './useExtensions';
import { ExtensionPointCallback } from './types';

interface ExtensionPointProps {
    name: string;
    context?: Context<{}>;
    options: Options;
    children: ExtensionPointCallback<PluginDescriptor>;
}

function ExtensionPoint({ name, context, options, children }: ExtensionPointProps) {
    if (typeof children !== 'function') {
        onDebug(({ error }) => {
            return {
                level: error,
                message: `ExtensionPoint expects a render-child.
The "children"-property passed for extension point "${name}" is not a function.`,
                components: 'ExtensionPoint',
                meta: {
                    name,
                    context,
                    options,
                },
            };
        });
        return null;
    }

    const [supportedDescriptors, unsupportedDescriptors, isLoading] = useExtensionsAll(name, context, options);
    return <>{children(supportedDescriptors, unsupportedDescriptors, isLoading)}</>;
}

export default ExtensionPoint;
