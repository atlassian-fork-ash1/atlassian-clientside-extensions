// eslint-disable-next-line max-classes-per-file
import React, { Fragment, useEffect, useMemo, useRef, useState } from 'react';
import ModalDialog, { ModalTransition } from '@atlaskit/modal-dialog';
import { onDebug } from '@atlassian/clientside-extensions-debug';

type MouseEvent = React.MouseEvent<HTMLElement> | React.KeyboardEvent<HTMLElement>;
type ModalUserAction = (e: MouseEvent) => void;
type ModalCloseAction = (e: MouseEvent) => void | boolean;

export enum ModalAppearance {
    'danger' = 'danger',
    'warning' = 'warning',
}

export enum ModalWidth {
    'small' = 'small',
    'medium' = 'medium',
    'large' = 'large',
    'x-large' = 'x-large',
}

export type ModalAction = {
    text: string;
    onClick: () => void;
    isDisabled?: boolean;
    isLoading?: boolean;
    testId?: string;
};

const noop = () => {};

type ModalRenderCallback = (element: HTMLElement, options: {}) => void;

type ModalCleanupCallback = (element: HTMLElement) => void;

export type MountProps = {
    options: {};
};

const ModalBody = ({ modalApi }: { modalApi: Api }) => {
    const ref = useRef<HTMLDivElement>(null);

    useEffect(() => {
        if (ref.current) {
            modalApi.getRenderCallback()(ref.current, {});
        }

        return () => {
            modalApi.getCleanupCallback()(ref.current);
        };
    }, [modalApi]);

    return <div ref={ref} />;
};

export class Api {
    private title = '';

    private width: ModalWidth;

    private appearance: ModalAppearance;

    private renderCallback: ModalRenderCallback = noop;

    private cleanupCallback: ModalCleanupCallback = noop;

    private readonly closeModalFn: () => void = noop;

    private onCloseCallback: ModalCloseAction = noop;

    constructor(closeModalFn: () => void, setActions: React.Dispatch<React.SetStateAction<ModalAction[]>>) {
        this.closeModalFn = closeModalFn;

        this.setActions = actions => {
            setActions(Api.filterActionProps(actions));

            return this;
        };
    }

    public setTitle = (title: string) => {
        this.title = title;

        return this;
    };

    public setWidth = (width: ModalWidth) => {
        // Runtime checking for 'non-ts' usage
        if (!(width in ModalWidth)) {
            onDebug(({ error }) => ({
                level: error,
                message: `Invalid value specified as "width" of modal. Please make sure to use one of the supported values.`,
                meta: {
                    value: width,
                    supported: Object.keys(ModalWidth).concat(),
                },
            }));
            return this;
        }

        this.width = width;
        return this;
    };

    public setAppearance = (appearance: ModalAppearance) => {
        // Runtime checking for 'non-ts' usage
        if (!(appearance in ModalAppearance)) {
            onDebug(({ error }) => ({
                level: error,
                message: `Invalid value specified as "appearance" of modal. Please make sure to use one of the supported values.`,
                meta: {
                    value: appearance,
                    supported: Object.keys(ModalAppearance).concat(),
                },
            }));
            return this;
        }

        this.appearance = appearance;
        return this;
    };

    public onMount = (callback: ModalRenderCallback) => {
        this.renderCallback = callback;

        return this;
    };

    public onUnmount = (callback: ModalCleanupCallback) => {
        this.cleanupCallback = callback;

        return this;
    };

    public closeModal = () => {
        this.closeModalFn();
    };

    public getTitle() {
        return this.title;
    }

    public getWidth() {
        return this.width;
    }

    public getAppearance() {
        return this.appearance;
    }

    public getRenderCallback() {
        return this.renderCallback;
    }

    public getCleanupCallback() {
        return this.cleanupCallback;
    }

    public setActions: (actions: ModalAction[]) => void;

    public onClose(callback: ModalCloseAction) {
        this.onCloseCallback = callback;

        return this;
    }

    public getOnCloseCallback() {
        return this.onCloseCallback;
    }

    static filterActionProps(actions: unknown[]): ModalAction[] {
        if (!Array.isArray(actions)) {
            return [];
        }

        return actions.map(({ text, onClick, isDisabled, isLoading, testId }) => {
            return { text, onClick, isDisabled, isLoading, testId };
        });
    }
}

export type ModalRenderExtension = (api: Api) => {};

interface ModalHandlerProps {
    render: ModalRenderExtension;
    isOpen: boolean;
    onClose: () => void;
    __withoutTransition?: boolean;
    __disableFocusLock?: boolean;
}

export const type = 'modal';

export const ModalRenderer = ({ render, isOpen, onClose, __withoutTransition = false, __disableFocusLock = false }: ModalHandlerProps) => {
    const [actions, setActions] = useState<ModalAction[]>([]);
    const modalApi = useMemo(() => {
        const api = new Api(onClose, setActions);
        render(api);
        return api;
    }, [render]);

    const Wrapper = __withoutTransition ? Fragment : ModalTransition;

    // Here will be dragons. That's a fishy solution for programmatically disabling the focus lock of the Atlaskit Modal component.
    // When the stackIndex > 0 this will disable the focus lock of the Modal. That's quite naive implementation since we are abusing
    // the Atlaskit stacking mechanism.
    const stackIndex = __disableFocusLock ? 1 : 0;

    return (
        <Wrapper>
            {isOpen && (
                <ModalDialog
                    testId="client-plugins-modal"
                    actions={actions}
                    heading={modalApi.getTitle()}
                    width={modalApi.getWidth()}
                    appearance={modalApi.getAppearance()}
                    onClose={e => {
                        const closeResult = modalApi.getOnCloseCallback()(e);
                        // prevent closing of modal if "closeRequest" is "false"
                        if (closeResult !== false) {
                            onClose();
                        }
                    }}
                    stackIndex={stackIndex}
                >
                    <ModalBody modalApi={modalApi} />
                </ModalDialog>
            )}
        </Wrapper>
    );
};
