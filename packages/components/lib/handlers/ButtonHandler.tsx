import React, { FunctionComponent } from 'react';
import { PluginDescriptor } from '@atlassian/clientside-extensions-registry';
import useDebug from '../debug/useDebug';

export const type = 'button';

export type ButtonRenderExtension = () => {};

export const ButtonRenderer: FunctionComponent<{ extension: PluginDescriptor }> = ({ extension }) => {
    const [debug] = useDebug();
    if (debug) {
        return <div>{JSON.stringify(extension)}</div>;
    }
    return null;
};
