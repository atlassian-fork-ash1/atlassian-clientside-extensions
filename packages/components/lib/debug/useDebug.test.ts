import { mocked } from 'ts-jest/utils';
import { act, renderHook } from '@testing-library/react-hooks';
import { isDebugEnabled, observeStateChange } from '@atlassian/clientside-extensions-debug';
import useDebug from './useDebug';

jest.mock('@atlassian/clientside-extensions-debug');

describe('useDebug hook', () => {
    beforeEach(() => {
        mocked(isDebugEnabled).mockClear();
        mocked(observeStateChange).mockClear();
    });

    it('should return the current state of the debug state', () => {
        const INITIAL_VALUE = false;
        mocked(isDebugEnabled).mockReturnValueOnce(INITIAL_VALUE);
        mocked(observeStateChange).mockReturnValueOnce({ unsubscribe() {} });

        const { result } = renderHook(() => useDebug());
        const [debugValue] = result.current;

        expect(debugValue).toBe(INITIAL_VALUE);
    });

    it('should react to changes to the discover debug setting', () => {
        const INITIAL_VALUE = true;
        const CHANGE_VALUE = false;
        let updateCallback: Parameters<typeof observeStateChange>[0];
        mocked(isDebugEnabled).mockReturnValueOnce(INITIAL_VALUE);
        mocked(observeStateChange).mockImplementation(expectedCallback => {
            updateCallback = expectedCallback;
            return {
                unsubscribe() {},
            };
        });

        const { result } = renderHook(() => useDebug());
        let [debugValue] = result.current;

        expect(debugValue).toBe(INITIAL_VALUE);

        act(() => {
            updateCallback({ discovery: CHANGE_VALUE, debug: CHANGE_VALUE, logging: CHANGE_VALUE, validation: CHANGE_VALUE });
        });

        [debugValue] = result.current;

        expect(debugValue).toBe(CHANGE_VALUE);
    });
});
