import { useEffect, useState } from 'react';
import { isDiscoveryEnabled, setDiscoveryEnabled, observeStateChange } from '@atlassian/clientside-extensions-debug';

export default () => {
    const [discoveryState, setDiscoveryState] = useState(isDiscoveryEnabled());

    useEffect(() => {
        const subscription = observeStateChange(newState => {
            setDiscoveryState(newState.discovery);
        });
        return () => subscription.unsubscribe();
    }, []);

    return [discoveryState, setDiscoveryEnabled] as [boolean, (value: boolean) => void];
};
