import { useEffect, useState } from 'react';
import { isLoggingEnabled, setLoggingEnabled, observeStateChange } from '@atlassian/clientside-extensions-debug';

export default () => {
    const [loggingState, setLoggingState] = useState(isLoggingEnabled());

    useEffect(() => {
        const subscription = observeStateChange(newState => {
            setLoggingState(newState.logging);
        });
        return () => subscription.unsubscribe();
    }, []);

    return [loggingState, setLoggingEnabled] as [boolean, (value: boolean) => void];
};
