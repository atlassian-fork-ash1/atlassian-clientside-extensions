---
title: Glossary - Client-side Extensions
platform: server
product: clientside-extensions
category: reference
subcategory: glossary
date: '2020-01-07'
---

# Glossary

Learn more about the terminology associated with the Atlassian client-side extensions.

## Client-side Extensions

<table>
    <colgroup>
        <col width="33%" />
        <col width="77%" />
    </colgroup>
    <thead>
        <tr class="header">
            <th>Term</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr class="odd">
            <td>Client-side Extensions</td>
            <td>A set of APIs that allow a plugin author to augment and extend an Atlassian product's runtime using client-side JavaScript code.</td>
        </tr>
        <tr class="odd">
            <td>Extension</td>
            <td>Is the implementation of a set of APIs to extend the UI of a product.</td>
        </tr>
        <tr class="even">
            <td>Extension point</td>
            <td>Is a specific location on the UI that can be extended in a product. The product decides when and how to render the extension, but can provide special
            containers where extensions can render custom content (see extension types for more)</td>
        </tr>
        <tr class="even">
            <td>Descriptor</td>
            <td>Metadata of the extension that is declared in the <code>atlassian-plugin.xml</code>. For now, we make use of the web-item plugin module to declare this metadata,
            but there’s a plan to provide a `clientside-extension` module in the future that provides better support for attributes.</td>
        </tr>
        <tr class="odd">
            <td>Entry Point (or entry-point)</td>
            <td>Refers to the JavaScript file where the factory of an extension is declared and registered.</td>
        </tr>
        <tr class="even">
            <td>Factory</td>
            <td>Consist of a closure function that returns a function which will be called by the plugin system to calculate the attributes of an extension.</td>
        </tr>
        <tr class="odd">
            <td>Attributes</td>
            <td>An object that defines properties that then are bound to the extension once rendered by the product. This attributes can be dynamic, and can also depend on context shared by products at runtime.</td>
        </tr>
        <tr class="even">
            <td>Type</td>
            <td>Extensions can have a type (link, button, modal, panel), which then affects how are they going to be rendered and react to user interaction.</td>
        </tr>
        <tr class="odd">
            <td>The registry</td>
            <td>Implements the browser runtime for the client-side extensions system, keeping track of extensions and extension points present in the screen.</td>
        </tr>
        <tr class="even">
            <td>The runtime</td>
            <td>A P2 plugin that provides the REST endpoint from which the plugin system gathers the extension descriptors, and also provides a singleton of the registry.</td>
        </tr>
        <tr class="odd">
            <td>The components</td>
            <td>An npm package provided to products for an easy implementation of client-side extensions using react.</td>
        </tr>
        <tr class="even">
            <td>The webpack plugin</td>
            <td>A webpack plugin provided to Vendors for an easy consumption of client-side extensions API.</td>
        </tr>
    </tbody>
</table>

## Atlassian Server plugin system

<table>
    <colgroup>
        <col width="33%" />
        <col width="33%" />
        <col width="33%" />
    </colgroup>
    <thead>
        <tr class="header">
            <th>Term</th>
            <th>Description</th>
            <th>References</th>
        </tr>
    </thead>
    <tbody>
        <tr class="odd">
            <td>Product</td>
            <td>Any product listed under “Server / Data Center products” section of this page:</td>
            <td><a href=" https://www.atlassian.com/legal/privacy-policy/product-family">product-family</a></td>
        </tr>
            <tr class="even">
            <td>Plugin</td>
            <td>A plugin is a bundle of code, resources and configuration files that can be dropped into an Atlassian product to add new functionality or change the behaviour of existing features.</td>
            <td>
                <a href="https://developer.atlassian.com/server/framework/gadgets/plugin-framework-2/">plugin-framework-2</a>
            </td>
        </tr>
        <tr class="odd">
            <td>P2</td>
            <td>
                <p>The Atlassian Plugin Framework 2 is the latest development framework allowing developers to build plugins for Atlassian application.</p>
                <p>P2 plugins are powered by OSGi and Spring.</p>
            </td>
            <td>
                <a href="https://developer.atlassian.com/server/framework/gadgets/plugin-framework-2/">plugin-framework-2</a>
            </td>
        <tr class="odd">
            <td>Module Descriptor</td>
            <td>A Java class responsible for interpreting direct children of an <code>atlassian-plugin</code> XML file, converting them in to usable primitives in Java business logic.</td>
            <td><p></p></td>
        </tr>
            <tr class="even">
            <td>Descriptor</td>
            <td>Often used to refer to metadata of a web-fragment.</td>
            <td><p></p></td>
        </tr>
        <tr class="odd">
            <td>Plugin Modules</td>
            <td>An Atlassian plugin can implement one or more plugin modules to affect the underlying Atlassian application.</td>
            <td><a href="https://developer.atlassian.com/server/framework/atlassian-sdk/plugin-modules/">plugin-modules</a></td>
        </tr>
        <tr class="even">
            <td>Web Fragments</td>
            <td>A web fragment is a link, a section of links or an area of HTML content (or 'panel') in a particular location of a Server Product web interface.</td>
            <td><a href="https://developer.atlassian.com/server/bitbucket/reference/web-fragments/" >web-fragments</a></td>
        </tr>
        <tr class="odd">
            <td>- web-item</td>
            <td>Web Item plugin modules allow plugins to define new links in application menus.</td>
            <td><a href="https://developer.atlassian.com/server/framework/atlassian-sdk/web-item-plugin-module/">web-item-plugin-module</a></td>
        </tr>
        <tr class="even">
            <td>- web-section</td>
            <td>Web Section plugin modules allow plugins to define new sections in application menus. Each section can contain one or more links.</td>
            <td><a href="https://developer.atlassian.com/server/framework/atlassian-sdk/web-section-plugin-module/">web-section-plugin-module</a></td>
        </tr>
        <tr class="odd">
            <td>- web-panel</td>
            <td>Web Panel plugin modules allow plugins to define panels, or sections, on an HTML page. A panel is a set of HTML that will be inserted into a page.</td>
            <td><a href="https://developer.atlassian.com/server/framework/atlassian-sdk/web-panel-plugin-module/">web-panel-plugin-module</a></td>
        </tr>
        <tr class="even">
            <td>- “location” / “section” attribute</td>
            <td>
                <p>Denotes the place where the fragment should be rendered. Products are responsible for consuming the things registered here.</p>
                <p>WebPanels and WebSections call it “location”.</p>
                <p>WebItems call it “section”, mostly because they can also be rendered inside web-sections.</p>
            </td>
            <td><p></p></td>
        </tr>
        <tr class="odd">
            <td>Web Resources</td>
            <td>Web Resource plugin modules allow plugins to define downloadable resources.</td>
            <td><a href="https://developer.atlassian.com/server/framework/atlassian-sdk/web-resource-plugin-module/">web-resource-plugin-module</a></td>
        </tr>
    </tbody>
</table>
