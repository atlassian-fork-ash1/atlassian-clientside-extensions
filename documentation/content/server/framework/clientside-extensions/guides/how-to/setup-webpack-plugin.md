---
title: Setup CSE webpack plugin - Client-side Extensions
platform: server
product: clientside-extensions
category: devguide
subcategory: how-to
date: '2020-02-25'
---

# Setting up the CSE webpack plugin

You can create Client-side Extensions (CSE) without webpack, but the use of the provided webpack plugin is strongly advised in order
to improve your development experience.

This plugin will generate all the Atlassian XML descriptors needed for your extensions to work
correctly inside an Atlassian Plugin, and let you focus on writing your front end without worrying about implementation details.

## 1. Installation

Add `@atlassian/clientside-extension-webpack-plugin` as a dependency of your project:

```
npm install --save-dev @atlassian/clientside-extensions-webpack-plugin
```

And make sure to also install its peer dependency: `atlassian-webresource-webpack-plugin`

```
npm install --save-dev atlassian-webresource-webpack-plugin
```

## 2. Configuring CSE

In your webpack configuration, import `@atlassian/clientside-extensions-webpack-plugin` and use as follows:

```js
// #webpack.config.js
const path = require('path');
const CSEPlugin = require('@atlassian/clientside-extensions-webpack-plugin');

const csePlugin = new CSEPlugin({
    // configure a working directory where the plugin will look for your extensions
    cwd: path.join(__dirname, './src/main/frontend'),

    // use a pattern to specify which files should be considered as extensions
    pattern: '**/extensions/**/*.js',
});

module.exports = {
    // the CSE webpack plugin will generate all the entry-point definitions for you
    entry: {
        ...csePlugin.generateEntrypoints(),
    },

    // Add it to the list of plugins
    plugins: [csePlugin],

    // we will configure this later...
    output: {
        ...
    },
};
```

## 3. Configuring WRM webpack plugin

CSE webpack plugin makes use of [WRM webpack plugin](https://bitbucket.org/atlassianlabs/atlassian-webresource-webpack-plugin/src) to generate the
resource definitions for your extensions, so you also need to configure it.

In your webpack configuration, import `atlassian-webresource-webpack-plugin` and use as follows:

```js
// #webpack.config.js
const path = require('path');
const WrmPlugin = require('atlassian-webresource-webpack-plugin');
const CSEPlugin = require('@atlassian/clientside-extensions-webpack-plugin');

const csePlugin = new CSEPlugin({...});

// All your JavaScript code should be generated inside your maven project’s build output directory, which is usually target/classes.
const outputDirectory = path.join(__dirname, 'target', 'classes');

// Products provide clientside-extensions-registry as an AMD module.
// Set it as a provide dependency in order to avoid bundling the registry in your code.
const providedDependencies = {
    '@atlassian/clientside-extensions-registry': {
        dependency: 'com.atlassian.plugins.atlassian-clientside-extensions-runtime:runtime',
        import: {
            var: "require('@atlassian/clientside-extensions-registry')",
            amd: '@atlassian/clientside-extensions-registry',
        },
    },
};

/**
 * Configure WRM webpack plugin
 *
 * More info about WRM Webpack Plugin:
 * https://bitbucket.org/atlassianlabs/atlassian-webresource-webpack-plugin
 */
const wrmPlugin = new WrmPlugin({
    // Your Atlassian Plugin key
    pluginKey: 'com.example.plugin',

    // Path where WRM Plugin will store the generated XML descriptors for you.
    xmlDescriptors: path.resolve(outputDirectory, 'META-INF', 'plugin-descriptors', 'wr-webpack-bundles.xml'),

    // Set the list of provided dependencies
    providedDependencies: providedDependencies,
});


module.exports = {
    entry: {...},

    // Add WRM to the list of plugins
    plugins: [wrmPlugin, csePlugin],

    // configure your output to point to the target directory of your Atlassian Plugin.
    output: {
        path: outputDirectory,
        filename: '[name].bundle.js',
    },
};
```

## 4. Final configuration

At the end, your webpack configuration should look very similar to the following example:

```js
// #webpack.config.js
const path = require('path');
const CSEPlugin = require('@atlassian/clientside-extensions-webpack-plugin');
const WrmPlugin = require('atlassian-webresource-webpack-plugin');

const targetDirectory = path.join(__dirname, 'target');

const providedDependencies = {
    '@atlassian/clientside-extensions-registry': {
        dependency: 'com.atlassian.plugins.atlassian-clientside-extensions-runtime:runtime',
        import: {
            var: "require('@atlassian/clientside-extensions-registry')",
            amd: '@atlassian/clientside-extensions-registry',
        },
    },
};

const wrmPlugin = new WrmPlugin({
    pluginKey: 'com.example.plugin',
    xmlDescriptors: path.resolve(outputDirectory, 'META-INF', 'plugin-descriptors', 'wr-webpack-bundles.xml'),
    providedDependencies: providedDependencies,
});

const csePlugin = new CSEPlugin({
    cwd: path.join(__dirname, './src/main/frontend'),
    pattern: '**/extensions/**/*.js',
});

module.exports = {
    entry: {
        ...csePlugin.generateEntrypoints(),
    },
    plugins: [wrmPlugin, clientPlugin],
    output: {
        path: outputDirectory,
        filename: '[name].bundle.js',
    },
};
```
